import { STORE_KEY_ENUM } from '@/enum/store-key'
// 存储变量名
let storageKey = 'snowy-mobile-unitui'
// 存储节点变量名
let storageNodeKeys = [...Object.values(STORE_KEY_ENUM)]
// 存储的数据
let storageData = uni.getStorageSync(storageKey) || {}
const storage = {
	set: function(key, value) {
		if (storageNodeKeys.indexOf(key) != -1) {
			let tmp = uni.getStorageSync(storageKey)
			tmp = tmp ? tmp : {}
			tmp[key] = value
			uni.setStorageSync(storageKey, tmp)
		}
	},
	get: function(key) {
		return storageData[key] || ""
	},
	remove: function(key) {
		delete storageData[key]
		uni.setStorageSync(storageKey, storageData)
	},
	clean: function() {
		uni.removeStorageSync(storageKey)
	}
}
export default storage