import App from './App'
import './interceptor'
import { createSSRApp } from 'vue'
import store from './store'
import XEUtils from '@/plugins/xe-utils'
import snowy from '@/plugins/snowy'
import propsConfig from '@/components/thorui/thorui/tui-config/index.js'
//解决控制台 touchstart 事件警告
import 'default-passive-events'
export function createApp() {
	const app = createSSRApp(App)
	app.use(store)
	app.config.globalProperties.$snowy = snowy
	app.config.globalProperties.$xeu = XEUtils
	
	uni.$snowy = snowy
	uni.$xeu = XEUtils
	uni.$tui = propsConfig
	return {
		app
	}
}